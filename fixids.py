import json

with open("items.json") as f:
    item_data = json.load(f)

    new_ids = {}
    item_ids = [item["id"] for item in item_data.values()]

    for item in item_data.values():
        new_ids[item["path"]] = item["id"]

    old_ids = {}
    with open("ids.json", "r") as f2:
        old_ids = json.load(f2)

    combined_ids = {}
    for path in old_ids.keys():
        if not new_ids.get(path):
            combined_ids[path] = old_ids[path]
            # print(f"Missing path: {path}")
        else:
            combined_ids[path] = new_ids[path]

    with open("ids3.json", "w") as f2:
        # sort by value
        sorted_ids = {
            k: v for k, v in sorted(combined_ids.items(), key=lambda item: item[1])
        }
        json.dump(sorted_ids, f2, indent="\t")
